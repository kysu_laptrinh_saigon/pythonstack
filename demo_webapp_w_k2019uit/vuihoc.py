from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def index():
    return "<p>Hello, World vuihoc!</p>"

@app.route("/vuihoc")
def vuihoc():
    return render_template('vuihoc.html')

@app.route("/vuihoccothamso")
def vuihoccothamso():
    return render_template('vuihoc.html', chuongtrinh=333)


DIEM = 10

@app.route("/vuihoccothamso2")
def vuihoccothamso2():
    chuongtrinh = 4444
    temp = 'NHINH'
    d = DIEM
    return render_template('vuihoc.html', **locals(), **globals() )
