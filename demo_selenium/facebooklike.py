from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
#
from selenium.webdriver.common.by import By
#
import os
from dotenv import load_dotenv
import time


WD_IMPLICITLY_WAIT=6

def load_webdriver_f_autoinstall_chrome_wd():  # f aka from
    #region bypass permission popup ref. https://stackoverflow.com/a/39997484/248616  ref2. https://www.programcreek.com/python/example/100025/selenium.webdriver.ChromeOptions
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-notifications")
    #endregion

    wd = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)

    wd.implicitly_wait(WD_IMPLICITLY_WAIT)
    wd.maximize_window()
    return wd
wd = load_webdriver_f_autoinstall_chrome_wd()  # wd aka webdriver

#region load envvar
THIS_DIR = os.path.abspath(os.path.dirname(__file__))
load_dotenv(dotenv_path=f'{THIS_DIR}/.env')  # take environment variables from .env.
FB_USER = os.environ.get('FB_USER')
FB_PASS = os.environ.get('FB_PASS')
#endregion load envvar


try:
    wd.get('https://fb.com')

    # get element email e_email --> e_email.sendkeys()
    e = wd.find_element(By.XPATH, '//*[@id="email"]')
    e.send_keys(FB_USER)

    e = wd.find_element(By.XPATH, '//*[@id="pass"]')
    e.send_keys(FB_PASS)

    wd.get('https://fb.com/namgivu')

    like_e__all = wd.find_elements(By.XPATH, "//span[contains(text(),'Like')]")
    n = len(like_e__all)
    for i in range(2, n+1):
        xp     = f"(//span[contains(text(),'Like')])[{i}]"
        like_e = wd.find_element(By.XPATH, xp)
        like_e.click()

    pass

except:
    raise
finally:
    wd.quit()
