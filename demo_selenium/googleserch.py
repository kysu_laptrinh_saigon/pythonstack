from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
#
from selenium.webdriver.common.by import By


WD_IMPLICITLY_WAIT=6

def load_webdriver_f_autoinstall_chrome_wd():  # f aka from
    #region bypass permission popup ref. https://stackoverflow.com/a/39997484/248616  ref2. https://www.programcreek.com/python/example/100025/selenium.webdriver.ChromeOptions
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--disable-notifications")
    #endregion

    wd = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=chrome_options)

    wd.implicitly_wait(WD_IMPLICITLY_WAIT)
    wd.maximize_window()
    return wd


wd = load_webdriver_f_autoinstall_chrome_wd()  # wd aka webdriver

try:
    wd.get('https://google.com')

    # wd.get('https://stackoverflow.com')  #TODO scrape :stackoverflow after :googlesearch

    '''
    locate searchbox to enter keyword to search
    
    use chrome browser inspector to get xpath 
    //input[@title]
    '''
    e = wd.find_element(By.XPATH, '//input[@title]')
    e.send_keys('selenium')
    e.send_keys('\n')

    '''
    got searchresult page displayed --> get header+link of some results ontop 
    '''
    result = []

    xpath_h3_all = "//div[@data-hveid]//a/h3"
    # /html/body/ntp-app//div/ntp-realbox//div/input
    # //*[@id="input"]

    h3_all       = wd.find_elements(By.XPATH, xpath_h3_all)
    for i in range(1, len(h3_all) + 1):
        xpath_h3 = f"({xpath_h3_all})[{i}]"
        # eg (//div[@data-hveid]//a/h3)[1]
        # eg (//div[@data-hveid]//a/h3)[2]


        h3      = wd.find_element(By.XPATH, xpath_h3)
        h3_text = h3.text.strip()

        result.append(h3_text)

    print(result)
    from pprint import pprint
    pprint(result)

except:
    raise

finally:
    wd.quit()  # CAUTION quit called is a MUST to close webdriver instance and free machine resource
